//
//  CentralVC.swift
//  BTChat
//
//  Created by Albert Puła on 14.10.2017.
//  Copyright © 2017 Blue Book. All rights reserved.
//

import UIKit
import CoreBluetooth

class CentralVC: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager: CBCentralManager?
    var discoveredPeripherals = [CBPeripheral]()
    var connectedPeripheral: CBPeripheral?
    
    var messageCharacteristic: CBCharacteristic?
    
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var sendBt: UIButton!
    
    @IBAction func sendButtonClicked() {
        let updatedValue = textField!.text!
        let updatedValueData = updatedValue.data(using: String.Encoding.utf8)
        
        connectedPeripheral!.writeValue(updatedValueData!, for: messageCharacteristic!, type: CBCharacteristicWriteType.withResponse)
        textField?.text?.removeAll()
    }
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        sendBt.isEnabled = false
        centralManager = CBCentralManager.init(delegate: self, queue: nil, options: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func connectToPeripheral() {
        for discovered in discoveredPeripherals {

            print("Connecting to peripheral: \(discovered.identifier)")
            
            connectedPeripheral = discovered
            centralManager!.connect(connectedPeripheral!, options: nil)
            return
        }
    }
    
    // MARK: CBCentralManagerDelegate
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state != .poweredOn {
            print(central.state)
            return
        }

        centralManager!.scanForPeripherals(withServices: [myServiceUUID], options: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        var duplicatePeripheral = false
        for discovered in discoveredPeripherals {
            if (discovered.identifier == peripheral.identifier) {
                duplicatePeripheral = true
            }
        }
        if (!duplicatePeripheral) {
            print("didDiscover: \(peripheral), \(RSSI)" )
            self.discoveredPeripherals.append(peripheral)
            
            centralManager!.stopScan()
            connectToPeripheral()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("didConnect: \(peripheral)")
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        sendBt.isEnabled = true
    }
    
    // MARK: CBPeripheralDelegate
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            print("didDiscoverService: \(service)")
            
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            print("didDiscoverCharacteristicsFor \(service): \(characteristic)")
            
            if (characteristic.uuid == myCharacteristicUUID) {
                peripheral.setNotifyValue(true, for: characteristic)
                messageCharacteristic = characteristic
                
                peripheral.readValue(for: characteristic)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if let data = characteristic.value {
            let dataString = String(data: data, encoding: String.Encoding.utf8)!
            print("Characteristic data for \(characteristic): \(dataString)")
            
            if characteristic.uuid == myCharacteristicUUID {
                messageLabel!.text = dataString
            }
        }
        
        print("Characteristic data for \(characteristic): data is nil.")
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if (error != nil) {
            print(error!)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if (error != nil) {
            print(error!)
        }
    }
}
