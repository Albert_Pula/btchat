//
//  PeripheralVC.swift
//  BTChat
//
//  Created by Albert Puła on 14.10.2017.
//  Copyright © 2017 Blue Book. All rights reserved.
//

import UIKit
import CoreBluetooth

class PeripheralVC: UIViewController, CBPeripheralManagerDelegate {
    
    var peripheralManager: CBPeripheralManager?
    var characteristic: CBMutableCharacteristic?
    var service: CBMutableService?
    
    var subscribedCharacteristic: CBCharacteristic?
    
    
    let message = "Hello from peripheral :)"
    
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var sendBt: UIButton!
    
    @IBAction func sendButtonClicked() {
        let updatedValue = textField!.text!
        let updatedValueData = updatedValue.data(using: String.Encoding.utf8)
        
        let didSendValue = peripheralManager?.updateValue(updatedValueData!, for: characteristic!, onSubscribedCentrals: nil)
        print("didSendValue: \(String(describing: didSendValue))")
        textField?.text?.removeAll()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        sendBt.isEnabled = false
        peripheralManager = CBPeripheralManager.init(delegate: self, queue: nil, options: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidDisappear(_ animated: Bool) {
        peripheralManager?.stopAdvertising()
    }
    
    // MARK: CBPeripheralManagerDelegate
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if (peripheral.state != .poweredOn) {
            print(peripheral.state)
            return
        }
        
        
        characteristic = CBMutableCharacteristic.init(type: myCharacteristicUUID,
                                                        properties: [CBCharacteristicProperties.read,
                                                                     CBCharacteristicProperties.write,
                                                                     CBCharacteristicProperties.notify],
                                                        value: nil,
                                                        permissions: [CBAttributePermissions.readable,
                                                                      CBAttributePermissions.writeable])
        service = CBMutableService.init(type: myServiceUUID, primary: true)
        
        if var characteristics = service?.characteristics {
            characteristics.append(characteristic!)
        } else {
            service!.characteristics = [characteristic!]
        }
        peripheralManager!.add(service!)
        
        peripheralManager!.startAdvertising([CBAdvertisementDataServiceUUIDsKey : [service!.uuid]])
        
        print("startAdvertising.")
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        if (error != nil) {
            print(error!)
        }
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        if (error != nil) {
            print(error!)
        }
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        print("didReceiveRead: \(request)")
        
        let myValueData = message.data(using: String.Encoding.utf8)
        
        if (request.characteristic.uuid == characteristic!.uuid) {
           
            request.value = myValueData!
            peripheralManager?.respond(to: request, withResult: CBATTError.success)
            print("respond: \(request)")
        }
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        

        characteristic!.value = requests.first!.value
        
        print("didReceiveWrite requests: \(String(describing: characteristic!.value))")
        let myNewValue = String(data: characteristic!.value!, encoding: String.Encoding.utf8)!
        messageLabel!.text = myNewValue
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        print("\(central) didSubscribeTo: \(characteristic)")
        
        sendBt.isEnabled = true
    }
    
}


