//
//  MainVC.swift
//  BTChat
//
//  Created by Albert Puła on 14.10.2017.
//  Copyright © 2017 Blue Book. All rights reserved.
//

import UIKit


class MainVC: UIViewController {

    @IBAction func centalClick(_ sender: Any) {
        performSegue(withIdentifier: "centralSegue", sender: nil)
    }
    
    @IBAction func peripheralClick(_ sender: Any) {
       
        let storyboard = UIStoryboard(name: "PeripheralView", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "PeripheralVC")
       
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

}


